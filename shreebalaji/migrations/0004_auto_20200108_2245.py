# Generated by Django 2.2.9 on 2020-01-08 17:00

import ckeditor.fields
import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shreebalaji', '0003_auto_20200108_1034'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactUs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('subject', models.CharField(max_length=50)),
                ('message', models.TextField()),
            ],
            options={
                'verbose_name_plural': 'ContactUs',
            },
        ),
        migrations.CreateModel(
            name='ProjectStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'ProjectStatus',
            },
        ),
        migrations.AlterField(
            model_name='gallery',
            name='publish_date',
            field=models.DateTimeField(verbose_name=datetime.datetime(2020, 1, 8, 22, 45, 54, 704209)),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='ongoingProjects')),
                ('descriptions', ckeditor.fields.RichTextField()),
                ('status', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shreebalaji.ProjectStatus')),
            ],
        ),
    ]
